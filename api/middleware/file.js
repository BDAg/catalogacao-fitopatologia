const multer = require('multer');

// DESTINATIONS
const storageCrops = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/crops')
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString()+file.originalname);
    }
});

const storageDiseases = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/diseases')
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString()+file.originalname);
    }
});

// FILTERS
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    };
};

// UPLOADS
const crops = multer({
    storage: storageCrops,
    limits: {fileSize: 1024 * 1024 * 5},
    fileFilter: fileFilter
});

const diseases = multer({
    storage: storageDiseases,
    limits: {fileSize: 1024 * 1024 * 10},
    fileFilter: fileFilter
});


// ALTERNATIVES
exports.uploadCrops = crops.single('imagem_cultura');

exports.uploadDiseases = diseases.single('imagem_doenca');