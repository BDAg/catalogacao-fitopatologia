var mysql = require('../../../mysql').pool;

exports.toViewDisease = (req, res, next) => {

    const queryViewDisease = `SELECT * FROM Doencas WHERE id = ${req.params.id}`;

    mysql.getConnection((error, conn) => {
        conn.query(queryViewDisease, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status === 0) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Fitopatologia encontra-se desabilitada.'
                    });
                }

                return res.status(200).json({
                    message: 'Fitopatologia encontrada com sucesso.',
                    dados: {
                        id_doenca:                      results[0].id,
                        nome_comum:                     results[0].nome_comum,
                        nome_cientifico:                results[0].nome_cientifico,
                        nome_sexuado:                   results[0].nome_sexuado,
                        nome_assexuado:                 results[0].nome_assexuado,
                        tipo:                           results[0].tipo,
                        sintomas:                       results[0].sintomas,
                        umidade:                        results[0].umidade,
                        periodo_molhamento:             results[0].periodo_molhamento,
                        temperatura:                    results[0].temperatura,
                        agente_etiologico:              results[0].agente_etiologico,
                        icone:                          "http://localhost:3000/"+results[0].icone
                    }
                });
            } else {
                return res.status(401).json({
                    error:      null,
                    message:    'Falha na autenticação.'
                });
            };
        });
    });
};

exports.toEditDisease = (req, res, next) => {

    if (!req.body.id_cultura) {
        return res.status(401).json({
            error:      null,
            message:    'Você precisa inserir qual é o id da cultura da fitopatologia.'
        });
    };

    var id_doenca           = req.params.id;
    var nomeComum           = '';
    var nomeCientifico      = '';
    var nomeSexuado         = '';
    var nomeAssexuado       = '';
    var tipo                = '';
    var sintomas            = '';
    var umidade             = '';
    var periodoMolhamento   = '';
    var temperatura         = '';
    var agenteEtiologico    = '';
    var icone               = '';
    var status              =  1;

    if (req.body.nome_comum)                { nomeComum = req.body.nome_comum };
    if (req.body.nome_cientifico)           { nomeCientifico = req.body.nome_cientifico };
    if (req.body.nome_sexuado)              { nomeSexuado = req.body.nome_sexuado };
    if (req.body.nome_assexuado)            { nomeAssexuado = req.body.nome_assexuado };
    if (req.body.tipo)                      { tipo = req.body.tipo };
    if (req.body.sintomas)                  { sintomas = req.body.sintomas };
    if (req.body.umidade)                   { umidade = req.body.umidade };
    if (req.body.periodo_molhamento)        { periodoMolhamento = req.body.periodo_molhamento };
    if (req.body.temperatura)               { temperatura = req.body.temperatura };
    if (req.body.agente_etiologico)         { agenteEtiologico = req.body.agente_etiologico };
    if (req.file)             { icone = req.file.path };

    var queryCheckDisease = `SELECT * FROM Doencas 
                                       WHERE id                 = '${id_doenca}' AND 
                                             id_cultura         =  ${req.body.id_cultura}`;

    mysql.getConnection((error, conn) => {
        conn.query(queryCheckDisease, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha de autenticação.zap'
                    });
                };

                if (nomeComum == '')                { nomeComum = results[0].nome_comum };
                if (nomeCientifico == '')           { nomeCientifico = results[0].nome_cientifico };
                if (nomeSexuado == '')              { nomeSexuado = results[0].icone };
                if (nomeAssexuado == '')            { nomeAssexuado = results[0].nome_assexuado };
                if (tipo == '')                     { tipo = results[0].tipo };
                if (sintomas == '')                 { sintomas = results[0].sintomas };
                if (umidade == '')                  { umidade = results[0].umidade };
                if (periodoMolhamento == '')        { periodoMolhamento = results[0].periodo_molhamento };
                if (temperatura == '')              { temperatura = results[0].temperatura };
                if (agenteEtiologico == '')         { agenteEtiologico = results[0].agente_etiologico };
                if (icone == '')                    { icone = results[0].icone };

                var queryEditDisease = `  UPDATE Doencas
                                             SET nome_comum             = '${nomeComum}',
                                                 nome_cientifico        = '${nomeCientifico}',
                                                 nome_sexuado           = '${nomeSexuado}',
                                                 nome_assexuado         = '${nomeAssexuado}',
                                                 tipo                   = '${tipo}',
                                                 sintomas               = '${sintomas}',
                                                 umidade                = '${umidade}',
                                                 periodo_molhamento     = '${periodoMolhamento}',
                                                 temperatura            = '${temperatura}',
                                                 agente_etiologico      = '${agenteEtiologico}',
                                                 icone                  = '${icone}',
                                                 status                 =  ${status}
                                           WHERE id                     =  ${id_doenca}`;

                conn.query(queryEditDisease, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(200).json({
                            error:      null,
                            message:    'Fitopatologia atualizada com sucesso.'
                        });
                    } else {
                        return res.status(401).json({
                            error:      null,
                            message:    'Falha de autenticação.'
                        });
                    };
                });
            } else {
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};


exports.toDisableDisease = (req, res, next) => {

    if (!req.body.nome_cientifico) {
        return res.status(401).json({
            error:      null,
            message:    'É necessario informar o nome da fitopatologia.'
        });  
    };

    var queryDisableDisease = `UPDATE Doencas
                                  SET status      = 0
                                WHERE id          = ${req.params.id}`;

    const queryCheckDisase = `SELECT * FROM Doencas 
                                      WHERE nome_cientifico = '${req.body.nome_cientifico}' AND 
                                            id = ${req.params.id}`

    mysql.getConnection((error, conn) => {
        conn.query(queryCheckDisase, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status == 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa fitopatologia já se encontra desativada.'
                    });
                };
                conn.query(queryDisableDisease, results[0].id, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(200).json({
                            error:      null,
                            message:    'Fitopatologia desativada com sucesso.'
                        });
                    } else {
                        return res.status(500).json({
                            error:      null,
                            message:    'Houve uma falha inesperada.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.diseasesList = (req, res, next) => {

    var queryDiseasesList = `SELECT * FROM Doencas where status = 1`;

    mysql.getConnection((error, conn) => {
        conn.query(queryDiseasesList, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };

                let DiseaseList = [];

                results.forEach(doenca => {
                    DiseaseList.push({
                        id_doenca:                      doenca.id,
                        id_cultura:                     doenca.id_cultura,
                        nome_comum:                     doenca.nome_comum,
                        nome_cientifico:                doenca.nome_cientifico,
                        nome_sexuado:                   doenca.nome_sexuado,
                        nome_assexuado:                 doenca.nome_assexuado,
                        tipo:                           doenca.tipo,
                        sintomas:                       doenca.sintomas,
                        umidade:                        doenca.umidade,
                        periodo_molhamento:             doenca.periodo_molhamento,
                        temperatura:                    doenca.temperatura,
                        agente_etiologico:              doenca.agente_etiologico,
                        icone:                          "http://localhost:3000/"+doenca.icone
                    });
                });

                return res.status(200).json({
                    error:      null,
                    message:    'Fitopatologias encontrada com sucesso.',
                    dados:      DiseaseList
                });
            } else {
                return res.status(401).json({
                    error:      error,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.diseasesByCrop = (req, res, next) => {

    var queryDiseasesByCrop = `SELECT * FROM Doencas 
                                       WHERE id_cultura = ${req.params.id} AND 
                                             status = 1`;

    mysql.getConnection((error, conn) => {
        conn.query(queryDiseasesByCrop, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };

                let DiseaseList = [];

                results.forEach(doenca => {
                    DiseaseList.push({
                        id_doenca:                      doenca.id,
                        nome_comum:                     doenca.nome_comum,
                        nome_cientifico:                doenca.nome_cientifico,
                        nome_sexuado:                   doenca.nome_sexuado,
                        nome_assexuado:                 doenca.nome_assexuado,
                        tipo:                           doenca.tipo,
                        sintomas:                       doenca.sintomas,
                        umidade:                        doenca.umidade,
                        periodo_molhamento:             doenca.periodo_molhamento,
                        temperatura:                    doenca.temperatura,
                        agente_etiologico:              doenca.agente_etiologico,
                        icone:                          "http://localhost:3000/"+doenca.icone
                    });
                });

                return res.status(200).json({
                    error:      null,
                    message:    'Fitopatologias encontrada com sucesso.',
                    dados:      DiseaseList
                });
            } else {
                return res.status(401).json({
                    error:      error,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.register = (req, res, next) => {

    if (!req.body.id_cultura || !req.body.nome_cientifico || !req.body.nome_comum || !req.body.tipo) {
        return res.status(401).json({
            error:      null,
            message:    'É necessário informar no mínimo o id da cultura, nome comum, nome científico e o tipo.'
        });
    };

    var nomeComum           = '';
    var nomeCientifico      = '';
    var nomeSexuado         = '';
    var nomeAssexuado       = '';
    var tipo                = '';
    var sintomas            = '';
    var umidade             = '';
    var periodoMolhamento   = '';
    var temperatura         = '';
    var agenteEtiologico    = '';
    var icone               = '';
    var status              =  1;
    var idCultura           = req.body.id_cultura;

    if (req.body.nome_comum)                { nomeComum = req.body.nome_comum };
    if (req.body.nome_cientifico)           { nomeCientifico = req.body.nome_cientifico };
    if (req.body.nome_sexuado)              { nomeSexuado = req.body.nome_sexuado };
    if (req.body.nome_assexuado)            { nomeAssexuado = req.body.nome_assexuado };
    if (req.body.tipo)                      { tipo = req.body.tipo };
    if (req.body.sintomas)                  { sintomas = req.body.sintomas };
    if (req.body.umidade)                   { umidade = req.body.umidade };
    if (req.body.periodo_molhamento)        { periodoMolhamento = req.body.periodo_molhamento };
    if (req.body.temperatura)               { temperatura = req.body.temperatura };
    if (req.body.agente_etiologico)         { agenteEtiologico = req.body.agente_etiologico };
    if (req.file.path)                      { icone = req.file.path };

    var queryRegisterDisease = `INSERT INTO 
                                       Doencas (nome_comum,
                                                nome_cientifico, 
                                                nome_sexuado, 
                                                nome_assexuado, 
                                                tipo, 
                                                sintomas, 
                                                umidade, 
                                                periodo_molhamento, 
                                                temperatura, 
                                                agente_etiologico, 
                                                id_cultura, 
                                                icone,
                                                status) 
                                        VALUES  ('${nomeComum}', 
                                                 '${nomeCientifico}', 
                                                 '${nomeSexuado}', 
                                                 '${nomeAssexuado}', 
                                                 '${tipo}', 
                                                 '${sintomas}', 
                                                 '${umidade}', 
                                                 '${periodoMolhamento}', 
                                                 '${temperatura}', 
                                                 '${agenteEtiologico}', 
                                                  ${idCultura}, 
                                                 '${icone}', 
                                                 1)`;
    

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT 1 FROM Doencas WHERE nome_cientifico LIKE '${nomeCientifico}'`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length > 0) {
                    conn.release();
                    return res.status(401).json({
                        message: 'Fitopatologia já cadastrada.'
                    });
                };

                conn.query(queryRegisterDisease, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } 
                    if (results1) {
                        return res.status(201).json({
                            error:      null,
                            message:    'Fitopatologia cadastrada com sucesso.'
                        });
                    } else {
                        return res.status(401).json({
                            error:      null,
                            message:    'Falha de autenticação.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      error,
                    message:    'Falha na autenticação.'
                });
            };
        });
    });
};
