const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var mysql = require('../../mysql').pool;


exports.registerUser = (req, res, next) => {

    const queryRegisterUser = `INSERT INTO 
                             Usuarios (nome, email, senha, status)
                               VALUES ('${req.body.nome}', '${req.body.email}', ?, 1)`;
    

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT 1 FROM Usuarios WHERE email = '${req.body.email}'`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } else {
                if (results.length > 0) {
                    conn.release();
                    return res.status(409).json({
                        error:      null,
                        message:    'Email já cadastrado.'
                    });
                } else {
                    bcrypt.hash(req.body.senha, 10, (err, hash) => {
                        if (err) {
                            conn.release();
                            return res.status(500).json({
                                error:      err,
                                message:    'Erro de encriptação'
                            });
                        } else {
                            conn.query(queryRegisterUser, hash, (error1, results1, fields1) => {
                                conn.release();
                                if (error1) {
                                    return res.status(500).json({
                                        error:      error1,
                                        message:    'Erro ao conectar no banco.'
                                    });
                                } else {
                                    return res.status(201).json({
                                        error:      null,
                                        message:    'Usuário incluído com sucesso.',
                                        id:         results1.insertId
                                    })
                                };
                            });
                        };
                    });
                };
            };
        });
    });
};

exports.toViewProfile = (req, res, next) => {

    var queryViewProfile = `SELECT * FROM Usuarios WHERE id = ${req.params.id}`;

    mysql.getConnection((error, conn) => {
        conn.query(queryViewProfile, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } else {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };

                if (results[0].status == 0) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa esta desativada.'
                    });
                };

                return res.status(200).json({
                    error:      null,
                    message:    'Usuario encontrado com sucesso.',
                    dados: {
                        nome:           results[0].nome,
                        email:          results[0].email
                    }
                });
            };
        });
    });
};

exports.toEditProfile = (req, res, next) => {

    if(!req.body.senha_atual){
        return res.status(406).json({
            error:      null,
            message:    'O corpo precisa ter a senha atual e os dados a serem atualizados.'
        });
    };

    var idUsuario       = req.params.id;
    var nome            = '';
    var email           = '';
    var senha           = '';
    var status          =  1;

    if (req.body.nome)          { nome = req.body.nome };
    if (req.body.email)         { email = req.body.email };
    if (req.body.senha)         { senha = req.body.senha };

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT * FROM Usuarios WHERE id = ${req.params.id}`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                bcrypt.compare(req.body.senha_atual, results[0].senha, (err, result) => {
                    if (err) {
                        conn.release();
                        return res.status(401).json({
                            error:      err,
                            message:    'Falha de autenticação.'
                        });
                    } 
                    if (result) {
                        if (req.body.email) {
                            conn.query(`SELECT 1 FROM Usuarios WHERE email = '${req.body.email}'`, (error1, results1, fields1) => {
                                if (error1) {
                                    conn.release();
                                    return res.status(500).json({
                                        error:      error1,
                                        message:    'Erro ao conectar no banco.'
                                    });
                                }
                                if (results1.length > 0) {
                                    conn.release();
                                    return res.status(409).json({
                                        error:      null,
                                        message:    'Email já cadastrado.'
                                    });
                                };
                            });
                        };
                        if (req.body.senha) {
                            bcrypt.hash(senha, 10, (err1, hash) => {
                                if (err1) {
                                    conn.release();
                                    return res.status(500).json({
                                        error:      err1,
                                        message:    'Erro de encriptação.'
                                    });
                                } else {

                                    if (nome == '')         { nome = results[0].nome };
                                    if (email == '')        { email = results[0].email };

                                    var queryEditPassword = `UPDATE Usuarios
                                                                 SET nome       = '${nome}',
                                                                     email      = '${email}',
                                                                     senha      = '${hash}',
                                                                     status     =  ${status}
                                                               WHERE id         =  ${idUsuario}`;

                                    conn.query(queryEditPassword, (error2, results2, fields2) => {
                                        conn.release();
                                        if (error2) {
                                            return res.status(500).json({
                                                error:      error2,
                                                message:    'Não foi possível atualizar os dados da conta.'
                                            });
                                        };
                                        if (results2) {
                                            return res.status(200).json({
                                                error:      null,
                                                message:    'Os dados foram atualizados com sucesso.'
                                            });
                                        } else {
                                            return res.status(401).json({
                                                error:      error,
                                                message:    'Falha de autenticação.'
                                            });
                                        }
                                        
                                    });
                                };
                            });
                        } else {

                            if (nome == '')         { nome = results[0].nome };
                            if (email == '')        { email = results[0].email };

                            var queryEditProfile = ` UPDATE Usuarios
                                                        SET nome       = '${nome}',
                                                            email      = '${email}',
                                                            status     =  ${status}
                                                      WHERE id         =  ${idUsuario}`;

                            conn.query(queryEditProfile, (error3, results3, fields3) => {
                                conn.release();
                                if (error3) {
                                    return res.status(500).json({
                                        error:      error3,
                                        message:    'Não foi possível atualizar os dados da conta.'
                                    });
                                };
                                if (results3) {
                                    return res.status(200).json({
                                        error:      null,
                                        message:    'Os dados foram atualizados com sucesso.'
                                    });
                                } else {
                                    return res.status(401).json({
                                        error:      error,
                                        message:    'Falha de autenticação.'
                                    });
                                }
                                
                            });
                        };
                    } else {
                        conn.release();
                        return res.status(401).json({
                            error:      null,
                            message:    'Senha incorreta.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      error,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.toDisableProfile = (req, res, next) => {

    const queryDisableProfile = `UPDATE Usuarios
                                    SET status      = 0
                                  WHERE id          = ?`;

    mysql.getConnection((error, conn) => {
        conn.query(`SELECT * FROM Usuarios WHERE id = ${req.params.id}`, (error, results, fields) => {
            if (error) {
                conn.release();
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } 
            if (results) {
                if (results.length < 1) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status == 0) {
                    conn.release();
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa conta já se encontra desativada.'
                    });
                };
                conn.query(queryDisableProfile, results[0].id, (error1, results1, fields1) => {
                    conn.release();
                    if (error1) {
                        conn.release();
                        return res.status(500).json({
                            error:      error1,
                            message:    'Erro ao conectar no banco.'
                        });
                    } else {
                        return res.status(200).json({
                            error:      null,
                            message:    'Conta desativada com sucesso.'
                        });
                    };
                });
            } else {
                conn.release();
                return res.status(401).json({
                    error:      null,
                    message:    'Falha de autenticação.'
                });
            };
        });
    });
};

exports.login = (req, res, next) => {

    const queryLogin = `SELECT * FROM Usuarios WHERE email = '${req.body.email}'`;

    mysql.getConnection((error, conn) => {
        conn.query(queryLogin, (error, results, fields) => {
            conn.release();
            if (error) {
                return res.status(500).json({
                    error:      error,
                    message:    'Erro ao conectar no banco.'
                });
            } else {
                if (results.length < 1) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Falha na autenticação.'
                    });
                };
                if (results[0].status == 0) {
                    return res.status(401).json({
                        error:      null,
                        message:    'Essa conta esta desativada.'
                    });
                };
                bcrypt.compare(req.body.senha, results[0].senha, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            error:      err,
                            message:    'Falha na autenticação.'
                        });
                    };
                    if (result) {

                        const token = jwt.sign(
                            {
                                id_usuario:     results[0].id,
                                nome:           results[0].nome,
                                email:          results[0].email,
                                status:         results[0].status
                            }, process.env.JWT_KEY
                        );

                        return res.status(200).json({
                            error:      null,
                            message:    'Login realizado com sucesso.',
                            token:      token,
                            dados: {
                                id_usuario:     results[0].id,
                                nome:           results[0].nome,
                                email:          results[0].email,
                                status:         results[0].status
                            }
                        });
                    } else {
                        return res.status(401).json({
                            error:      null,
                            message:    'Senha incorreta.'
                        });
                    };
                });
            };
        });
    });
};