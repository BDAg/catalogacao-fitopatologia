const express = require('express');
const router = express.Router();

const controller = require('.././/../../controllers/crops/diseases/diseases-controller');
const login = require('../../../middleware/login');
const file = require('../../../middleware/file')

router.post('/register', login.requiredUser, file.uploadDiseases, controller.register);

router.get('/list/:id', controller.diseasesByCrop);

router.get('/:id', controller.toViewDisease);
router.patch('/:id', login.requiredUser, controller.toEditDisease);
router.delete('/:id', login.requiredUser, controller.toDisableDisease);

router.get('/', controller.diseasesList);

module.exports = router;