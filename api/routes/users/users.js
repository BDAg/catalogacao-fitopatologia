const express = require('express');
const router = express.Router();

const controller = require('../../controllers/users/users-controllers');
const login = require('../../middleware/login');

router.post('/register', controller.registerUser);

router.post('/login', controller.login);

router.get('/profile/:id', controller.toViewProfile);
router.patch('/profile/:id', login.requiredUser, controller.toEditProfile);
router.delete('/profile/:id', login.requiredUser, controller.toDisableProfile);

module.exports = router;