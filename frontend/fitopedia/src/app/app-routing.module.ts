import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layouts/layout/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { LayoutModule } from './layouts/layout.module';
import { CadastroUsuarioComponent } from './pages/cadastros/cadastro-usuario/cadastro-usuario.component';
import { CadastroCulturaComponent } from './pages/cadastros/cadastro-cultura/cadastro-cultura.component';
import { CadastroFitopatologiaComponent } from './pages/cadastros/cadastro-fitopatologia/cadastro-fitopatologia.component';
import { EditarUsuarioComponent } from './pages/modificacoes/editar-usuario/editar-usuario.component';
import { EditarCulturaComponent } from './pages/modificacoes/editar-cultura/editar-cultura.component';
import { EditarFitopatologiaComponent } from './pages/modificacoes/editar-fitopatologia/editar-fitopatologia.component';
import { ListaCulturasComponent } from './pages/culturas/lista-culturas/lista-culturas.component';
import { ListaFitopatologiaComponent } from './pages/culturas/fitopatologias/lista-fitopatologia/lista-fitopatologia.component';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FitopatologiaComponent } from './pages/culturas/fitopatologias/fitopatologia/fitopatologia.component';

const rotas: Routes = [

  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '*', redirectTo: 'home', pathMatch: 'full'},

  { 
    path: '', component: LayoutComponent, children: [
      {path: 'home', component: HomeComponent},
      {path: 'registrar', component: CadastroUsuarioComponent},
      {path: 'nova-cultura', component: CadastroCulturaComponent},
      {path: 'nova-fitopatologia', component: CadastroFitopatologiaComponent},
      {path: 'editar-usuario', component: EditarUsuarioComponent},
      {path: 'editar-cultura/:idCultura', component: EditarCulturaComponent},
      {path: 'editar-fitopatologia/:idFito', component: EditarFitopatologiaComponent},
      {path: 'culturas', component: ListaCulturasComponent},
      {path: 'fitopatologias/:idCultura', component: ListaFitopatologiaComponent},
      {path: 'login', component: LoginComponent},
      {path: 'fito/:idCultura/:idFito', component: FitopatologiaComponent}
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(rotas),
    LayoutModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    HomeComponent,
    CadastroCulturaComponent,
    CadastroFitopatologiaComponent,
    CadastroUsuarioComponent,
    EditarCulturaComponent,
    EditarUsuarioComponent,
    EditarFitopatologiaComponent,
    ListaCulturasComponent,
    ListaFitopatologiaComponent,
    LoginComponent,
    FitopatologiaComponent
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
