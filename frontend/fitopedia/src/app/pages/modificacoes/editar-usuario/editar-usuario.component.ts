import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;
  editarForm: FormGroup;

  dadosParaAtualizar = {};

  @ViewChild('fecharModal') fecharModal: ElementRef;

  loading = false;
  dadosAtualizadosComSucesso = false;
  falhaNaApi = false;
  senhasDivergindo = false;
  senhaInvalida = false;
  senhaErrada = false;
  emailJaCadastrado = false;
  nomeInvalido = false;
  emailInvalido = false;
  senhaNovaInvalida = false;

  get form() { return this.editarForm.controls; }

  constructor(
    private apiService: ApiService,
    private _router: Router,
    private formBuilder: FormBuilder
  ) {
    this.editarForm = this.formBuilder.group({
      nome: ['', Validators.minLength(3)],
      email: ['', Validators.email],
      senha_antiga : ['', Validators.required],
      senha_nova: ['', Validators.minLength(6)],
      senha_novaRep: ['', Validators.minLength(6)]
    });
   }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(!this.usuario){
      this._router.navigate(['/home']);
    }
  }

  checarNome() {
    if (this.form.nome.valid || this.form.nome.value == '') {
      this.nomeInvalido = false;
      if (this.form.nome.value != '') {
        this.dadosParaAtualizar['nome'] = this.form.nome.value;
      } else {
        delete this.dadosParaAtualizar['nome'];
      };
    } else {
      return this.nomeInvalido = true;
    };
  }

  checarEmail() {
    if (this.form.email.valid || this.form.email.value == '') {
      this.emailInvalido = false;
      if (this.form.email.value != '') {
        this.dadosParaAtualizar['email'] = this.form.email.value;
      } else {
        delete this.dadosParaAtualizar['email'];
      };
    } else {
      return this.emailInvalido = true;
    };
  }

  checarSenha(senha) {
    if (this.form[senha].valid) {
      this.senhaNovaInvalida = false;
      if (this.form[senha].value == '') {
        delete this.dadosParaAtualizar['senha'];
      }
    } else {
      this.senhaNovaInvalida = true;
    };
  }

  atualizarDados() {
    this.loading = true;
    if (this.form.senha_nova.value != this.form.senha_novaRep.value) {
      this.loading = false;
      return this.senhasDivergindo = true;
    };
    this.dadosParaAtualizar['senha_atual'] = this.form.senha_antiga.value;

    if (this.form.senha_nova.value != '') {
      this.dadosParaAtualizar['senha'] = this.form.senha_nova.value;
    }
     
    const subscription = this.apiService.editarPerfil(this.dadosParaAtualizar, this.usuario.id).subscribe(data => {
      this.loading = false;
      if ('nome' in this.dadosParaAtualizar) {
        this.usuario.nome = this.dadosParaAtualizar['nome'];
      };
      if ('email' in this.dadosParaAtualizar) {
        this.usuario.email = this.dadosParaAtualizar['email'];
      };
      localStorage.setItem('currentUser', JSON.stringify(this.usuario));
      this.dadosAtualizadosComSucesso = true;
      setTimeout(() => {
        window.location.reload();
      }, 3000)
    }, error => {
      this.loading = false;
      if (error.error.message = 'Senha incorreta.') {
        this.senhaErrada = true;
      } 
      if (error.error.message = 'Email já cadastrado.') {
        this.emailJaCadastrado = true;
      } else {
        this.falhaNaApi = true;
      };
    });
    this.subscription.add(subscription);
  }

  desativarConta() {
    console.log('oieeee');
    const subscription = this.apiService.desativarUsuario(this.usuario.id).subscribe(data => {
      console.log('oieeee2');
      this.usuario = null;
      localStorage.removeItem("currentUser");
      window.location.reload();
    }, error => {
      console.log('oieeee2');
      if (error.error.message = 'Senha incorreta.') {
        this.senhaErrada = true;
      } else {
        console.log(error.error.message);
        this.falhaNaApi = true;
      };
      this.fecharModal.nativeElement.click();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
