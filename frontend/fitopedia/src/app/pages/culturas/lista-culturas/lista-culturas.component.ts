import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/core/api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-lista-culturas',
  templateUrl: './lista-culturas.component.html',
  styleUrls: ['./lista-culturas.component.css']
})
export class ListaCulturasComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;

  culturas = [];

  falhaNaApi = false;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };

    const subscription = this.apiService.listarCulturas().subscribe(data =>{
      this.culturas = data['dados'];
    }, error =>{
      this.falhaNaApi = true;
    });
    this.subscription.add(subscription);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
