import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/core/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-lista-fitopatologia',
  templateUrl: './lista-fitopatologia.component.html',
  styleUrls: ['./lista-fitopatologia.component.css']
})
export class ListaFitopatologiaComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;

  id_cultura;
  doencas = [];
  cultura = {
    nome: '',
    descricao: ''
  };

  culturaDesativada = false;
  falhaNaApi = false;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };

    if (this.route.params['_value']['idCultura']) {
      this.id_cultura = this.route.params['_value']['idCultura'];
      this.buscaDadosDaCultura(this.id_cultura)
    };

  }

  buscaDadosDaCultura(id_cultura){

    const subscription = this.apiService.dadosCultura(id_cultura).subscribe(dados => {
      this.doencas = dados['dados']['doencas'];
      this.cultura = {
        nome: dados['dados']['nome'],
        descricao: dados['dados']['descricao']
      };
    }, error => {
      if (error.error.message == 'Essa cultura encontra-se desativada.') {
        this.culturaDesativada = true;
      } else {
        this.falhaNaApi = true;
      };
    });
    this.subscription.add(subscription);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
