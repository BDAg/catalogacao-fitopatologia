import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/core/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fitopatologia',
  templateUrl: './fitopatologia.component.html',
  styleUrls: ['./fitopatologia.component.css']
})
export class FitopatologiaComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  id_cultura;
  id_doenca;

  doenca = {};
  cultura = {};

  fitoDesabilitada = false;
  falhaNaApi = false;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    
    this.id_cultura = this.route.params['_value']['idCultura'];
    this.id_doenca = this.route.params['_value']['idFito'];
    this.buscaDadosDaDoenca(this.id_doenca);
    this.buscaDadosDaCultura(this.id_cultura);
  }

  buscaDadosDaDoenca(idDoenca) {
    const subscription = this.apiService.dadosFitopatologia(idDoenca).subscribe(data => {
      this.doenca = data['dados'];
    }, error => {
      if (error.error.message == 'Fitopatologia encontra-se desabilitada.') {
        this.fitoDesabilitada = true;
      } else {
        this.falhaNaApi = true;
      }
    });
    this.subscription.add(subscription);
  }

  buscaDadosDaCultura(idCultura) {
    const subscription = this.apiService.dadosCultura(idCultura).subscribe(data => {
      this.cultura = data['dados'];
    }, error => {
      console.log(error);
    });
    this.subscription.add(subscription);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
