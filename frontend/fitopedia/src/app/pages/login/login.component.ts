import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;
  loginForm: FormGroup;

  loading = false;
  falhaNaAutenticacao = false;
  falhaNaApi = false;
  contaDesativada = false;

  get form() { return this.loginForm.controls; }

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private _router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
      email:  ['', [Validators.required, Validators.email]],
      senha:  ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  realizarLogin() {

    this.loading = true;
  
    const dadosLogin = {
      email: this.form.email.value,
      senha: this.form.senha.value
    };
    
    const subscription = this.apiService.login(dadosLogin).subscribe(data =>{
      this.loading = false;
      if (data['error'] == null) {
        localStorage.setItem('currentUser', JSON.stringify({
          token: data["token"],
          id: data["dados"].id_usuario,
          email: data["dados"].email,
          nome: data["dados"].nome,
        }));
        window.location.reload();
      }
    }, error => {
      this.loading = false;
      if (error.error.error) {
        this.falhaNaApi = true;
      } else {
        if (error.error.message == "Essa conta esta desativada.") {
          this.contaDesativada = true;
        } else {
          this.falhaNaAutenticacao = true;
        };
      };
    });
    this.subscription.add(subscription);
  }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(this.usuario){
      this._router.navigate(['/home']);
    }
  }

  ngOnDestroy(){

    this.subscription.unsubscribe();
  }

}
