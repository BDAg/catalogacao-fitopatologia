import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/api.service';

@Component({
  selector: 'app-cadastro-fitopatologia',
  templateUrl: './cadastro-fitopatologia.component.html',
  styleUrls: ['./cadastro-fitopatologia.component.css']
})
export class CadastroFitopatologiaComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;
  cadastroForm: FormGroup;

  loading = false;
  falhaNaApi = false;
  sucessoCadastro = false;
  fitoJaCadastrada = false;

  get form() { return this.cadastroForm.controls };

  constructor(
    private formBuilder: FormBuilder,
    private _router: Router,
    private apiService: ApiService
  ) {
    this.cadastroForm = this.formBuilder.group({
      id_cultura:         ['',  [Validators.required]],
      nome_comum:         ['',  [Validators.required]],
      nome_cientifico:    ['',  [Validators.required]],
      bfv:                [''],
      imagem_doenca:      [null,[Validators.required]],
      nome_sexuado:       [''],
      nome_assexuado:     [''],
      sintomas:           [''],
      umidade:            [''],
      periodo_molhamento: [''],
      temperatura:        [''],
      agente_etiologico:  ['']
    })
    
   }

  onFileChange(event) {
    this.cadastroForm.patchValue({
      imagem_doenca: <File>event.target.files[0]
    });
  }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(!this.usuario){
      this._router.navigate(['/home']);
    }
  }

  cadastrarFito(){
    console.log(this.form.imagem_doenca.value);
    const formData = new FormData;
    formData.append('id_cultura', this.form.id_cultura.value);
    formData.append('nome_comum', this.form.nome_comum.value);
    formData.append('nome_cientifico', this.form.nome_cientifico.value);
    formData.append('imagem_doenca', this.form.imagem_doenca.value);
    formData.append('tipo', this.form.bfv.value);
    formData.append('nome_sexuado', this.form.nome_sexuado.value);
    formData.append('nome_assexuado', this.form.nome_assexuado.value);
    formData.append('sintomas', this.form.sintomas.value);
    formData.append('umidade', this.form.umidade.value);
    formData.append('periodo_molhamento', this.form.periodo_molhamento.value);
    formData.append('temperatura', this.form.temperatura.value);
    formData.append('agente_etiologico', this.form.agente_etiologico.value);
    const subscription = this.apiService.cadastrarFito(formData).subscribe(data => {
      console.log('DEU CERTO');
    }, error => {
      console.log(error.error);
    })
    this.subscription.add(subscription);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
