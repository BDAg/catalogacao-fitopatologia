import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/api.service';

@Component({
  selector: 'app-cadastro-cultura',
  templateUrl: './cadastro-cultura.component.html',
  styleUrls: ['./cadastro-cultura.component.css']
})
export class CadastroCulturaComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;
  cadastroForm: FormGroup;

  loading = false;
  falhaNaApi = false;
  sucessoCadastro = false;
  culturaJaCadastrada = false;

  get form() { return this.cadastroForm.controls };

  constructor(
    private formBuilder: FormBuilder,
    private _router: Router,
    private apiService: ApiService
  ) { 
    this.cadastroForm = this.formBuilder.group({
      nome:               ['',  [Validators.required]],
      descricao:          ['',  [Validators.required]],
      imagem_cultura:     [null,[Validators.required]]
    })
  }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(!this.usuario){
      this._router.navigate(['/home']);
    }
  }

  cadastrarCultura() {
    this.loading = true;
    console.log(this.cadastroForm.value);
    const subscription = this.apiService.cadastrarCultura(this.cadastroForm.value).subscribe(data => {
      this.loading = false;
      this.sucessoCadastro = true;
    }, error => {
      console.log(error);
      this.loading = false;
      if (error.error.error) {
        this.falhaNaApi = true;
      } else {
        if (error.error.message == "Cultura já cadastrada.") {
          this.culturaJaCadastrada = true;
        } else {
          console.log("Falha inesperada");
          this.falhaNaApi = true;
        }
      };
    });
  }

  onFileChange(event) {
    this.cadastroForm.patchValue({
      imagem_cultura: <File>event.target.files[0]
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
