import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/api.service';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})
export class CadastroUsuarioComponent implements OnInit, OnDestroy {

  usuario = JSON.parse(localStorage.getItem('currentUser'));

  subscription: Subscription;
  cadastroForm: FormGroup;

  dadosLogin;

  senhasDivergindo = false;
  emailJaCadastrado = false;
  sucessoCadastro = false;
  falhaNaApi =  false;
  loading = false;

  get form() { return this.cadastroForm.controls };

  constructor(
    private formBuilder: FormBuilder,
    private _router: Router,
    private apiService: ApiService
  ) {
    this.cadastroForm = this.formBuilder.group({
      nome:       ['', [Validators.required]],
      email:      ['', [Validators.required, Validators.email]],
      senha:      ['', [Validators.required, Validators.minLength(6)]],
      senhaRep:   ['', [Validators.required, Validators.minLength(6)]],
    });
   }

  realizarCadastro(){
    this.loading = true;
    // VERIFICA AS SENHAS INSERIDAS
    if (this.form.senha.value != this.form.senhaRep.value) {
      this.loading = false;
      return this.senhasDivergindo = true;
    };
    const dadosCadastro = {
      nome:   this.form.nome.value,
      email:  this.form.email.value,
      senha:  this.form.senha.value
    };

    const subscription = this.apiService.registrarUsuario(dadosCadastro).subscribe(data => {
      this.sucessoCadastro = true;
      this.dadosLogin = {
        email:  this.form.email.value,
        senha:  this.form.senha.value
      };
      this.realizarLogin();
    }, error => {
      this.loading = false;
      if (error.error.error) {
        this.falhaNaApi = true;
      } else {
        this.emailJaCadastrado = true;
      };
    });
    this.subscription.add(subscription);
  }

  realizarLogin(){
    const subscription = this.apiService.login(this.dadosLogin).subscribe(data => {
      localStorage.setItem('currentUser', JSON.stringify({
        token: data["token"].token,
        id: data["dados"].id_usuario,
        email: data["dados"].email,
        nome: data["dados"].nome,
      }));
      setTimeout(() => {
        window.location.reload();
      }, 3000)
    }, error => {
      this.loading = false;
      this.falhaNaApi = true;
    });
    this.subscription.add(subscription);
  }

  ngOnInit() {

    if (!this.subscription) {
      this.subscription = new Subscription(); 
    };
    if(this.usuario){
      this._router.navigate(['/home']);
    }
  }

  ngOnDestroy(){

    this.subscription.unsubscribe();
  }

}
