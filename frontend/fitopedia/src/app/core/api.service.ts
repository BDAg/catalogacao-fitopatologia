import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';

const usuario = JSON.parse(localStorage.getItem('currentUser'));

const cabecalhoPadrao = {
  headers: {
    'Authorization' : 'Bearer ' + (usuario ? usuario.token : '')
  }
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private API_URL = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  // METODOS PARA USUARIO
  public login(body) {
    return this.http.post(`${this.API_URL}/users/login`, body);
  }

  public registrarUsuario(body) {
    return this.http.post(`${this.API_URL}/users/register`, body);
  }

  public perfilUsuario(id_usuario) {
    return this.http.get(`${this.API_URL}/users/profile/${id_usuario}`);
  }

  public editarPerfil(body, id_usuario) {
    return this.http.patch(`${this.API_URL}/users/profile/${id_usuario}`, body, cabecalhoPadrao);
  }

  public desativarUsuario(id_usuario) {
    return this.http.delete(`${this.API_URL}/users/profile/${id_usuario}`, cabecalhoPadrao);
  }

  // METODOS PARA CULTURA
  public listarCulturas(){
    return this.http.get(`${this.API_URL}/crops`)
  }

  public dadosCultura(id_cultura) {
    return this.http.get(`${this.API_URL}/crops/${id_cultura}`)
  }

  public cadastrarCultura(body){
    const formData = new FormData;
    formData.append('nome', body.nome);
    formData.append('descricao', body.descricao);
    formData.append('imagem_cultura', body.imagem_cultura);
    return this.http.post(`${this.API_URL}/crops/register`, formData, cabecalhoPadrao)
  }

  // METODOS PARA FITOPATOLOGIA
  public dadosFitopatologia(id_fito) {
    return this.http.get(`${this.API_URL}/crops/diseases/${id_fito}`)
  }

  public cadastrarFito(form){
    return this.http.post(`${this.API_URL}/crops/diseases/register`, form, cabecalhoPadrao)
  }
}
